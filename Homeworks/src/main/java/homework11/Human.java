package homework11;

import java.util.Objects;

public class Human {
    private final String name;
    private final String lastName;
    private final String patronymic;
    private final String city;
    private final String street;
    private final String house;
    private final String flat;
    private final String numberPassport;


    public Human(String name, String lastName, String patronymic,
                 String city, String street, String house, String flat, String numberPassport) {
        this.name = name;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
        this.numberPassport = numberPassport;
    }

    @Override
    public String toString() {
        return '\n'+ lastName + ' ' + name + ' ' + patronymic + '\n' + "Паспорт:\n"
                +  "Серия: 6722 " + "Номер: " + numberPassport + '\n' + "Город "+ city + ", ул. "
                + street  + ", дом "+  house + ", квартира " + flat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return numberPassport.equals(human.numberPassport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberPassport);
    }
}
