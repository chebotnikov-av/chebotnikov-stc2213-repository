package homework11;

import java.util.Random;

public class List {
    static Random random=new Random();
    private static final String[] listName = {"Сергей", "Дмитрий", "Иван", "Василий", "Виталий",
            "Игорь", "Владислав", "Кирилл", "Илья", "Андрей",
            "Максим", "Михаил", "Артём", "Пётр", "Данил"};

    private static final String[] listLastName = {"Медведев", "Конев", "Козлов", "Петухов", "Орлов",
            "Баранов", "Соколов", "Суслов", "Быков", "Коровин",
            "Зайцев", "Волков", "Бобров", "Кошкин", "Лебедев"};

    private static final String[] patronymic = {"Петрович", "Львович", "Иванович", "Сергеевич", "Ильич",
            "Артемович", "Дмитриевич", "Андреевич", "Тарасович", "Васильевич",
            "Игорьевич", "Максимович", "Антонович", "Данилович", "Анатольевич"
    };

    private static final String [] house = {"10", "11", "12", "13", "14", "15", "16", "17",
            "18", "19", "20", "21", "22", "23", "24"};

    private static final String [] flat = {"101", "110", "121", "132", "144", "157", "162", "173",
            "180", "191", "120", "201", "32", "43", "14"};

    private static final String [] numberPassport = {"01-1234", "02-1234", "03-1234", "04-1234", "05-1234",
            "06-1234", "07-1234", "08-1234", "09-1234", "10-1234", "11-1234", "12-1234", "13-1234",
            "14-1234", "15-1234"};

    private static final String [] city = {"Москва", "Тюмень", "Казань", "Волгоград", "Новосибирск", "Самара",
            "Тюмень", "Пенза", "Пермь", "Саратов", "Сызрань", "Брянск", "Тверь", "Челябинск", "Уфа"};

    private static final String [] street = {"Центральная", "Мира", "Ленина", "Революции", "Интернациональная",
            "Дружбы", "К.Маркса", "Л.Толстого", "Пушкина", "Нефтяников", "Спортивная", "Дзержинского",
            "Чапаева", "Жукова", "Рокоссовского"};

    public static String getCity() {
        return city[random.nextInt(city.length)];
    }

    public static String getStreet() {
        return street[random.nextInt(street.length)];
    }

    public static String getFlat() {
        return flat[random.nextInt(flat.length)];
    }

    public static String getNumberPassport() {
        return numberPassport[random.nextInt(numberPassport.length)];
    }

    public static String getListName() {
        return listName[random.nextInt(listName.length)];
    }

    public static String getListLastName() {
        return listLastName[random.nextInt(listLastName.length)];
    }

    public static String getPatronymic() {
        return patronymic[random.nextInt(patronymic.length)];
    }

    public static String getHouse() {
        return house[random.nextInt(house.length)];
    }
}
