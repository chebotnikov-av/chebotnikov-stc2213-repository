package homework11;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Human[] humans= new Human[10];
        fullArray(humans);
        System.out.println(Arrays.toString(humans));
        comparison(humans);
    }
    //Заполняем массив рандомными паспортными данными
    public static void fullArray(Human[] humans) {
        for (int i = 0; i < humans.length; i++) {
            Human human = new Human(List.getListName(),List.getListLastName(), List.getPatronymic(),
                    List.getCity(), List.getStreet(), List.getHouse(), List.getFlat(),
                    List.getNumberPassport());
            humans [i] = human;
        }
    }
    // В полученном массиве находим дупликаты паспортов. Во внутреннем цикле j=i+1,
    // для уменьшения прохождения не нужных итераций и сравнений позади стоящих паспортов.
    //Break используется для выхода из внутреннего цикла (for j) и перехода в цикл for i,
    // дупликат обнаружен, а значит нет смысла итерироваться дальше.
    // Если паспорт под индексом 2 был эквивалентен паспорту под индексом 4,
    // то сравнение 4 паспорта со 2 не имеет смысла, если 6 паспорт будет дупликатом 2,
    // то при проверке 4 паспорта с 6 это обнаружится (6 паспорт эквивалентен 4, то он и
    // эквивалентен 2) и счетчик дупликатов инкрементируется.
    // Получается данный метод проверяет на эквивалентность все паспорта, проходя с каждой итерацией цикла
    // только по впереди стоящим индексам. Выдает количество дупликатов паспортов подразумевая,
    // что один является оригиналом (если похожих номеров 10, то дупликатов 9).
    public static void comparison (Human[] humans){
        int compare=0;
        for (int i = 0; i < humans.length; i++) {
            for (int j = i+1; j < humans.length; j++) {
                if (humans[i].equals(humans[j])) {
                    compare++;
                    break;
                }
            }
        }
        System.out.println("Обнаружено дупликатов " + compare);
    }
}


