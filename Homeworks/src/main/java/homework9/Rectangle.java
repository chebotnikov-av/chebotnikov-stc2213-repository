package homework9;

public class Rectangle extends Figure{
    public Rectangle(int x, int y) {
        super(x, y);
    }

    @Override
    public void getPerimeter() {
        int perimeter = (getX() + getY())*2;
        System.out.println("Perimeter="+ perimeter);
    }
}
