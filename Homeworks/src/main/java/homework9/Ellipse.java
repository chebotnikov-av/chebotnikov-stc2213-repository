package homework9;

public class Ellipse extends Figure{
    public Ellipse(int x, int y) {
        super(x, y);
    }

    @Override
    public void getPerimeter() {
        //Да-да это формула вычисления окружности эллипса
        double perimeter=4*(Math.PI*getX()*getY()+Math.pow(getX()-getY(), 2))/ (getX()+ getY()) ;
        System.out.printf("Perimeter=%.2f\n", perimeter); //данным способом отображает 2 знака после запятой
    }
}
