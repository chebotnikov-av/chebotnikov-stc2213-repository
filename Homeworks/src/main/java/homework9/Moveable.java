package homework9;

public interface Moveable {
    void move(int x, int y);
}
