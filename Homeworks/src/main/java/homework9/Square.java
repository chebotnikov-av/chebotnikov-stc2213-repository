package homework9;

public class Square extends Rectangle implements Moveable{
    public Square(int x, int y) {
        super(x, y);
    }

    @Override
    public void getPerimeter() {
        int perimeter = getX()*4;
        System.out.println("Perimeter="+ perimeter);
    }

    public void move(int x, int y) {
            System.out.println("x=" + getX()+" y="+getY());
            this.setX(x);
            this.setY(y);
            System.out.println("переместился на x="+getX()+" y="+getY());
    }
}
