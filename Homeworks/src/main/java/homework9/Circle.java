package homework9;

public class Circle extends Ellipse implements Moveable{
    public Circle(int x, int y) {
        super(x, y);
    }


    public void move(int x, int y) {
        System.out.println("x=" + getX()+" y="+getY());
        this.setX(x);
        this.setY(y);
        System.out.println("переместился на x="+getX()+" y="+getY());
    }

    @Override
    public void getPerimeter() {
        double perimeter = 2*Math.PI*getX(); //Координату Х принимаем за радиус
        System.out.printf("Perimeter=%.2f\n", perimeter); //данным способом отображает 2 знака после запятой
    }
}
