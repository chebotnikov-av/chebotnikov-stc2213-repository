package homework9;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random coordinates= new Random(); // Случайные координаты для перемещаемых фигур
        Figure[] figures = new Figure[4];// Массив всех фигур
        figures [0] = new Ellipse(5,9);
        figures [1] = new Rectangle(6, 6);
        figures [2] = new Circle(7, 7);
        figures [3] = new Square(4,4);

        Moveable [] moveables = new Moveable[4]; //Массив перемещаемых фигур
        moveables [0] = new Circle(4, 6);
        moveables [1] = new Square(8, 9);
        moveables [2] = new Circle(10, 8);
        moveables [3] = new Square(7, 13);

        // Цикл отображения координат и размеров фигур, размеры фигур взяты из задаваемых координат,
        // getSimpleName() дает имя класса фигуры
        for (int i = 0; i < figures.length; i++) {
            System.out.println( figures[i].getClass().getSimpleName() + " x=" + figures[i].getX() + " y="+ figures[i].getY()
            + " Height=" + figures[i].getX() +" Width="+ figures[i].getY());
        }
        System.out.println("***********************************");

        // Цикл отображения периметра фигур, значения X и Y будем использовать как значения ширины и высоты фигур
        //для окружности значение X принимаем как значение радиуса, значение Y не учитывается
        //для эллипса значение X принимаем как правую(левую) полуось, значение Y принимаем как верхнюю(нижнюю) полуось
        for (int i = 0; i < figures.length; i++) {
            System.out.print( figures[i].getClass().getSimpleName() + " ");
            figures[i].getPerimeter();
        }
        System.out.println("***********************************");

        // Цикл перемещения фигур, изначально отображаются начальные координаты фигур, потом координаты перемещения
        for (int i = 0; i < moveables.length; i++) {
            System.out.print( moveables[i].getClass().getSimpleName() + " ");
            moveables[i].move(coordinates.nextInt(21), coordinates.nextInt(21));
        }
    }
}
