package homework10;

public class Sequence {
    public static int[] filter(int[] array, ByCondition byCondition) {
        int[] number= new int[array.length];
        int j=0;
        for (int i = 0; i < array.length; i++) {
            //Если условие верно, то присваиваем в новый массив значение
            if (byCondition.isOk(array[i])){
                number[j]=array[i];
                j++;
            }
        }
        //создаем новый массив для копирования и убираем нулевые значения
        int[] even = new int[j];
        System.arraycopy(number, 0, even, 0, j);
        System.out.println("Количество значений в новом массиве " + j);
        return even;
    }
}
