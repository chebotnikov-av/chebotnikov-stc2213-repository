package homework10;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random var = new Random();
        int [] array = new int [9000000];
        for (int i = 0; i < array.length; i++) {
            array[i]= var.nextInt(2000000)+1000000;
        }

        //Решение пункт 1 проверка на четность элемента
        ByCondition condition_1= number -> number%2==0;
        int[] even= Sequence.filter(array, condition_1);
        System.out.println("Четные значения " + Arrays.toString(even));

        //Решение пункт 2 проверка, является ли сумма цифр элемента четным числом
        ByCondition condition_2= number -> {
            int sum=0;
            //Переводим число в символьный массив цифр
            String str=Integer.toString(number);
            char[] chars = str.toCharArray();
            //Каждый символ переводим в числовое значение, складываем и затем проверяем на четность сумму
            for (int i = 0; i < chars.length; i++) {
                int value=Integer.parseInt(String.valueOf(chars[i]));
                sum+=value;
            }
            return sum%2==0;
        };
        int[] even_2= Sequence.filter(array, condition_2);
        System.out.println("Сумма цифр элемента четная " +Arrays.toString(even_2));

        //Решение пункт 3 проверка на четность всех цифр числа
        ByCondition condition_3= number -> {
            int value;
            //Переводим число в символьный массив цифр
            String str = Integer.toString(number);
            char[] chars = str.toCharArray();
            //Разбираем каждый символ на четность, перед этим необходимо символ перевести в число
            for (int i = 0; i < chars.length; i++) {
                value = Integer.parseInt(String.valueOf(chars[i]));
                if (value%2!=0){
                    return false;
                }
            }
            return true;
        };
        int[] even_3= Sequence.filter(array, condition_3);
        System.out.println("Четность всех цифр числа " +Arrays.toString(even_3));

        //Решение пункт 4 проверка на палиндромность числа
        ByCondition condition_4= number -> {
            if(number>10) {
                //Переводим число в символьный массив цифр
                String str=Integer.toString(number);
                char[] chars = str.toCharArray();
                // В данном случае достаточно пройтись по половине массива,
                // затем проверяем равность значений противоположных элементов и производим сдвиг индексов,
                // также нет необходимости переводить символьные значения в числовые
                for (int i = 0, j= chars.length-1; i < chars.length/2; i++, j--) {
                    if (chars[i]!=chars[j]){
                        return false;
                    }
                }
                return true;
            }
            return false;
        };
        int[] palindrome= Sequence.filter(array, condition_4);
        System.out.println("Палиндром " + Arrays.toString(palindrome));
    }
}
