package homework6;
import java.util.Arrays;
import java.util.Random;

import static java.lang.Math.abs;

public class homework6 {
    public static void main(String[] args) {
        int[] array = new int[1000000]; //Создаем пустой массив 1 000 000 значений
        int[] repeatElement = new int[201]; //Создаем пустой массив для значений от -100 до 100
        int minimum; //переменная для определения минимального значения повторения в рандомном массиве
        fullArray(array); //Заполняем массив 1 000 000 случайными числами от -100 до 100
        sortVariable(array, repeatElement); //Распределяем значения рандомного массива
        minimum = searchMinRepeat(repeatElement); //Ищем минимальное значение повторений в рандомном массиве
        System.out.println("Минимум повторений "+minimum); //Вывод значений минимального повторения в рандомном массиве
        searchIndex(repeatElement, minimum); //Находим индекс минимального количества повторений, данный индекс является
        // числом из массива рандомных значений
    }

    //Метод, который заполняет массив случайными числами в диапазоне от -100 до 100 O(n) - линейная сложность
    public static void fullArray(int[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(201)-100 ;
        }
    }

    //Метод, который распределяет значения рандомного массива, согласно индекса в новом массиве
    public static void sortVariable(int [] array, int [] repeat) {
        int j; //индекс для нового массива
        for (int i=0; i< array.length; i++){
            //цикл инкрементирования значения индекса нового массива
            //для подсчета количества повторений зачений в массиве рандомных значений
            if (array[i]>=0) {
                j = array[i];
            }
            else { //для подсчета отрицательных значений используется модуль числа
                j=abs(array[i])+100; //для числа -50, получаем значени 50 и при сложении получаем индекс под номером 150
            }
            repeat[j]++;
        }
        System.out.println("Массив с повторяющимися значениями " + Arrays.toString(repeat));
    }
    // Метод поиска минимального значения повторений, минимальной переменной присваиваем максимально значение int,
    // в цикле проходимся по всем значениям и сравниваем с минимальной переменной, при обнаружении наименьшего числа
    // присваиваем данное значение переменной minVariable. Данный метод возвращает число с минимальным повторением значений.
    public static int searchMinRepeat (int[] repeatElement) {
        int minVariable = Integer.MAX_VALUE;
        for (int index = 0; index < repeatElement.length; index++) {
            if (repeatElement[index] < minVariable) {
                minVariable=repeatElement[index];
            }
        }
        return minVariable;
    }

    //Метод, который находит минимальное количество повторений, данный индекс является
    // числом из массива рандомных значений. Значения находящиеся после индекса 100, преобразуются со знаком '-',
    // если имеется несколько значений минимально-повторяющихся, то будет выведено каждое значение
    public static void searchIndex (int[] repeatElement, int minimum) { //
        for (int index = 0; index < repeatElement.length; index++) {
            if (minimum==repeatElement[index] && index<=100) {
                System.out.println("Минимально-повторяющееся значение "+index);
            }
            else if (minimum==repeatElement[index] && index>100) {
                System.out.println("Минимально-повторяющееся значение "+(-index+100)); // происходит обратное преобразование,
                // как было в методе sortVariable, если индекс 150, преобразуем его в число -150 прибавляем 100
                // и получаем значение -50
            }
        }
    }
}
