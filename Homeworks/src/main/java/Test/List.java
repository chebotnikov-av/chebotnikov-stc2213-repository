package Test;

import java.util.Random;

public class List {
    static Random random=new Random();
    private static final String[] listName = {"Сергей", "Дмитрий", "Иван", "Василий", "Виталий",
            "Игорь", "Владислав", "Кирилл", "Илья", "Андрей",
            "Максим", "Михаил", "Артём", "Пётр", "Данил"};

    private static final String[] listLastName = {"Медведев", "Конев", "Козлов", "Петухов", "Орлов",
            "Баранов", "Соколов", "Суслов", "Быков", "Коровин",
            "Зайцев", "Волков", "Бобров", "Кошкин", "Лебедев"};


    public static String getListName() {
        return listName[random.nextInt(listName.length)];
    }

    public static String getListLastName() {
        return listLastName[random.nextInt(listLastName.length)];
    }


}
