package Test;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        Random random = new Random();
        int value = random.nextInt(100) + 1;
        Humans[] listHumans = new Humans[value];
        fullArray(listHumans);
        System.out.println(Arrays.toString(listHumans));

    }

    public static void fullArray(Humans[] listHumans) {
        Random random = new Random();
        for (int i = 0; i < listHumans.length; i++) {
            int age = random.nextInt(100) + 1;
            Humans human = new Humans();
            listHumans[i]=human;
            listHumans[i].setName(List.getListName());
            listHumans[i].setLastName(List.getListLastName());
            listHumans[i].setAge(age);
        }
    }

}
