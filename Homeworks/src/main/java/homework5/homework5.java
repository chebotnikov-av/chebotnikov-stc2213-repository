package homework5;
import java.util.Arrays;
import java.util.Scanner;

public class homework5 {
    public static void main(String[] args) {
        System.out.println("Введите значение от 1 до 10");  //если ввести любое другое значение, метод выдаст -1
        Scanner scanner = new Scanner(System.in); //запрос на ввод искомого числа в массиве
        int number = scanner.nextInt(); // number - искомое значение в массиве
        int index_array; //index_array - переменная для обозначения индекса искомого значения,
        int[] array_1 = {5, 8, 10, 7, 3, 9, 2, 6, 4, 1}; // объявляем массив значений для метода поиска значений
        index_array=SearchIndex(array_1, number); //переменной index_array присваиваем возвращаемое значение метода
        System.out.println(index_array); //выводим результат на экран
        int[] array2 = {-5, 67, 0, 37, 0, 0, -4, 0, -43, 1}; //объявляем массив значений для метода переноса значений
        System.out.println(Arrays.toString(array2)); // распечатываем массив значений
        NumberLeft(array2); //вызываем метод переноса числовых значений

    }
    //Метод поиска заданного значения в массиве,
    // в данный метод передается массив и искомое значение
    // при нахождении заданного значения возвращает индекс элемента массива
    // если заданное число не найдется, возвращаемое значение будет -1
    private static int SearchIndex(int [] array, int a) {
        for (int index=0; index < array.length; index++) { //переменная index служит для прохождения по массиву данных
            if (a == array[index]) {//проверяется условие "если значение 'а' равно значению в массиве
                System.out.print("Index in array ");
                return index;
            }// возвращается индекс значения переменной в массиве
        }
        System.out.print("Error! Incorrect value ");
        return -1; // если значение не найдено возвращается -1
    }

    // Метод переноса числовых значений в левую часть массива, нулевые значения в правую часть массива,
    // в данный метод передается массив значений
    private static void NumberLeft(int[] array) {
        int[] array_number = new int[array.length]; // создается массив равной размерности переданному массиву
        int i, j; // переменные для прохождения по массивам данных
        for (i = 0, j=0; i < array_number.length; i++) {
            if(array[i]!=0) { //если значение массива не равно 0, то значение присваивается в новый массив
                array_number[j] = array[i];
                j++; // индекс нового массива значений увеличивается на 1, при выполнении условии цикла
            }
        }
        System.out.println(Arrays.toString(array_number)); //распечатываем массив - значимые переменные слева, 0 справа
    }

}

