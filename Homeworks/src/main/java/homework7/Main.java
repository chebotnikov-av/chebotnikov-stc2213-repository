package homework7;
import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
//Массивы данных для инициализации имени и фамилии
        String[] listName = {"Сергей", "Дмитрий", "Иван", "Василий", "Виталий",
                "Игорь", "Владислав", "Кирилл", "Илья", "Андрей",
                "Максим", "Михаил", "Артём", "Пётр", "Данил"};

        String[] listLastName = {"Медведев", "Конев", "Козлов", "Петухов", "Орлов",
                "Баранов", "Соколов", "Суслов", "Быков", "Коровин",
                "Зайцев", "Волков", "Бобров", "Кошкин", "Лебедев"};

        Random random = new Random();
        int value = random.nextInt(100) + 1;// Рандомно определяем размер массива до 100
        Humans[] listHumans = new Humans[value]; //Получение рандомного массива объектов Humans
        fullArray(listHumans, listName, listLastName); //Заполняем массив рандомными данными: Имя, Фамилия, Возраст
        System.out.println(Arrays.toString(listHumans));
        bubbleSort(listHumans);//Вызываем метод сортировки
        System.out.println(Arrays.toString(listHumans));// Распечатываем результат
    }

    public static void fullArray(Humans[] listHumans, String[] listName, String[] listLastName) {
        Random random = new Random();
        for (int i = 0; i < listHumans.length; i++) {
            int randomName = random.nextInt(15); // определяем индекс для заполнения имени объекта
            int randomLastName = random.nextInt(15); // определяем индекс для заполнения фамилии объекта
            int age = random.nextInt(100) + 1; //рандомное значения возраста, возраст начинается с 1 года
            String name = listName[randomName]; // строке имени присваиваем случайное имя из массива имен
            String lastName = listLastName[randomLastName]; //строке фамилия присваиваем случайную фамилию
                                                            // из массива фамилий
            Humans humans = new Humans(name, lastName, age);
            listHumans[i] = humans; //в массив заносим объекты humans
        }
    }

    // данной сортировкой определяем наименьший возраст человека, массив сортируется от младшего к старшему
    public static void bubbleSort (Humans[] listHumans){
        for (int i = 0; i< listHumans.length; i++ ) {
            for (int j=0; j< listHumans.length-1; j++) {
                if (listHumans[j].getAge() > listHumans[j+1].getAge()){
                    int tmpAge = listHumans[j].getAge(); //переменная для запоминания возраста
                    String tmpName = listHumans[j].getName(); //переменная для запоминания имени
                    String tmpLastName = listHumans[j].getLastName(); //переменная для запоминания фамилии
                    //меняем местами объекты в массиве
                    listHumans[j].setAge(listHumans[j+1].getAge());
                    listHumans[j].setName(listHumans[j+1].getName());
                    listHumans[j].setLastName(listHumans[j+1].getLastName());
                    listHumans[j+1].setAge(tmpAge);
                    listHumans[j+1].setName(tmpName);
                    listHumans[j+1].setLastName(tmpLastName);
                }
            }
        }
    }
}
