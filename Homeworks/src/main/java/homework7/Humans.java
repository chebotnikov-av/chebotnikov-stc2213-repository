package homework7;

public class Humans {
    private String name;
    private String lastName;
    private int age;

    public Humans(String name, String lastName, int age) {
        this.name = name;
        this.lastName=lastName;
        if (age < 0) {
            System.out.println("Неверное значение");
        }
        else
            this.age = age;
    }
    public Humans(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < 0) {
            System.out.println("Неверное значение");
        }
        else
            this.age = age;
    }

    @Override
    public String toString() {
        return "{" + name  +
                " " + lastName +
                ", age=" + age +
                '}';
    }
}
