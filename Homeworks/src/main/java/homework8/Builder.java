package homework8;

public class Builder extends Worker{
    public Builder(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        super.goToWork();// наследуем из предка для отображения ФИО и профессии
        System.out.println("Работает на стройке с 09-00 до 18-00, работа тяжелая и опасная");
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println("Отдыхает " + days + " дней: будет посещать музеи и выставки");
    }
}
