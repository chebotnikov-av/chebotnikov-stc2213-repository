package homework8;

public class Doctor extends Worker{
    public Doctor(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        super.goToWork();// наследуем из предка для отображения ФИО и профессии
        System.out.println("Работает ежедневно с 08-00 до 14-00, а также берёт ночные дежурства");
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println("Отдыхает " + days + " дней: уедет на море");
    }
}
