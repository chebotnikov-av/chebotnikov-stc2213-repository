package homework8;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random vacation= new Random();// используется для передачи количества дней отпуска
        Worker[] workers = new Worker[4];
        workers [0] = new Engineer("Иван", "Попов", "Инженер");
        workers [1] = new Doctor("Сергей", "Иванов", "Доктор");
        workers [2] = new Builder("Дмитрий", "Сидоров", "Строитель");
        workers [3] = new Driver("Игорь", "Смирнов", "Водитель");

        System.out.println("График работы сотрудников");
        for (int i=0; i< workers.length; i++) {
            workers[i].goToWork();
            System.out.println("*******************************");
        }
        System.out.println("График отпусков сотрудников:");
        for (int i=0; i< workers.length; i++){
            workers[i].goToVacation(vacation.nextInt(50)+30);
            System.out.println("*******************************");
        }
    }
}
