package homework8;

public class Driver extends Worker{
    public Driver(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        super.goToWork(); // наследуем из предка для отображения ФИО и профессии
        System.out.println("Работает ежедневно с 07-00 до 18-00, возит на стройку материалы");
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println("Отдыхает " + days + " дней: займется обучением");
    }
}
