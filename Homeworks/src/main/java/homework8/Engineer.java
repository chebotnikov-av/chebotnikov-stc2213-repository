package homework8;

public class Engineer extends Worker{
    public Engineer(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        super.goToWork(); // наследуем из предка для отображения ФИО и профессии
        System.out.println("Работает в офисе строительной компании с 08-00 до 17-00");
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println("Отдыхает " + days + " дней: уезжает к родителям");
    }
}
