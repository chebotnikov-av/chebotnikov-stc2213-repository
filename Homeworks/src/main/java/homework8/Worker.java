package homework8;

public class Worker {
    private String name;
    private String lastName;
    private String profession;

    public Worker(String name, String lastName, String profession) {
        this.name = name;
        this.lastName = lastName;
        this.profession = profession;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getProfession() {
        return profession;
    }

    public void goToWork(){
        System.out.println(getName()+" "+getLastName()+" профессия: "+ getProfession());
    }

    public void goToVacation(int days){
        System.out.println(getLastName()+ " " + getName() + " профессия: " + getProfession());
    }

}
